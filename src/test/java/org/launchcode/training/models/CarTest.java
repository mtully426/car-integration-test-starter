package org.launchcode.training.models;


import org.junit.Test;

import static org.junit.Assert.*;

public class CarTest {

    //constructor sets gasTankLevel properly
    @Test
    public void gasTankLevelDefaulted(){
        Car car = new Car("Toyota", "Prius", 10, 50);
        assertEquals("10.0", Double.toString(car.getGasTankLevel()));
    }


    //gasTankLevel is accurate after driving within tank range
    @Test
    public void gasTankLevelAccurateAfterDriving() {
        Car car = new Car("Toyota", "Prius", 10, 50);
        car.drive(250);
        assertEquals(5.0, car.getGasTankLevel(), .01);
    }

    //gasTankLevel is accurate after attempting to drive past tank range
    @Test
    public void gasTankLevelAccurateAfterDrivingPastRange() {
        Car car = new Car("Toyota", "Prius", 10, 50);
        car.drive(501);
        assertEquals(0, car.getGasTankLevel(), .01);
    }

    //can't have more gas than tank size, expect an exception
    @Test( expected = IllegalArgumentException.class)
    public void tryToOverFillTank() {
        Car car = new Car("Toyota", "Prius", 10, 50);
        car.setGasTankLevel(12);
    }

}
